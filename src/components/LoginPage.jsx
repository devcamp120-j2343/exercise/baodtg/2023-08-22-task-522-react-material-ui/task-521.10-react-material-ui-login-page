import { Box, Button, Container, Grid, InputAdornment, TextField } from "@mui/material"
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';


const LoginPage = () => {
    return (
        <div >
            <Container maxWidth={"100%"} >
                <Grid container>
                    <Grid item md={4} sx={{ height: "100vh", borderRight: "1px solid white", display: 'flex', backgroundColor:"#FFFFFF1A" }} >
                        <Box sx={{ display: 'flex', flexDirection: 'column', width: '50%', margin: "auto", }}>
                            <h1 style={{ color: "white", fontSize: '60px' }}>Login</h1>
                            <TextField variant="standard"
                                sx={{ borderBottom: "1px solid white" }}
                                label={"Username"}
                                InputLabelProps={{
                                    style: {
                                        color: 'white' // Set the label color here
                                    }
                                }}
                                InputProps={{
                                    style: {
                                        color: 'white' // Set the font color here
                                    },
                                   endAdornment: (
                                        <InputAdornment position="start">
                                            <PersonOutlineIcon sx={{color: "white"}}/>
                                        </InputAdornment>
                                    ),

                                }}

                            />
                            <TextField variant="standard"
                                label={"Password"}
                                sx={{ marginTop: "20px", borderBottom: "1px solid white" }}
                                InputLabelProps={{
                                    style: {
                                        color: 'white' // Set the label color here
                                    }
                                }}
                                InputProps={{
                                    style: {
                                        color: 'white' // Set the font color here
                                    },
                                    endAdornment: (
                                        <InputAdornment position="start">
                                            <VisibilityOffIcon sx={{color: "white"}}/>
                                        </InputAdornment>
                                    ),
                                }}
                            />
                            <Button variant="contained" sx={{background:"white", color: "black", marginTop:"35px", height:"45px", fontWeight: "700"}}>LOGIN </Button>

                        </Box>
                    </Grid>
                    <Grid item md={8} >

                    </Grid>
                </Grid>


            </Container>


        </div >

    )
}
export default LoginPage